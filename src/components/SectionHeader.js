import React from "react";
import "./SectionHeader.scss";

function SectionHeader(props) {
  // Render nothing if no title or subtitle
  if (!props.title && !props.subtitle) {
    return null;
  }

  return (
    <header
      className={
        "SectionHeader" + (props.className ? ` ${props.className}` : "")
      }
    >
      {props.title && (
        <>
          <h1
            className={
              "title has-text-weight-bold" +
              (props.size ? ` is-${props.size}` : "") +
              (props.size === 1 ? " is-size-2-mobile" : "") +
              (props.spaced ? " is-spaced" : "")
            }
          >
            {props.title}
          </h1>
          <h1 className="SectionHeader__no-classname">
            BrNOC je Brněnská přednášková noc, kde studenti přednášejí studentům
            o svých odborných pracích, zájmech anebo prostě o tématech, která
            jim připadají důležitá. Jedná se o největší akci svého druhu v ČR!
          </h1>
        </>
      )}
    </header>
  );
}

export default SectionHeader;
