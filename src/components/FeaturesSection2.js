import React from "react";
import Section from "./Section";
import Features2 from "./Features2";
import "./FeaturesSection2.scss";

function FeaturesSection2(props) {
  return (
    <Section
      color={props.color}
      size={props.size}
      backgroundImage={props.backgroundImage}
      backgroundImageOpacity={props.backgroundImageOpacity}
    >
      <h1 className="FeaturesSection2__title has-text-weight-bold has-text-centered title">
        O BrNOCi
      </h1>
      <div className="container">
        <Features2
          items={[
            {
              title: "Kdy",
              description:
                "Sedmý ročník se bude konat koncem letošního roku, datum ještě bude upřesněno. Můžete se těšit na úžasné přednášky, workshopy a jako již tradičně na Mystery room.",
              image: "https://uploads.divjoy.com/undraw-mind_map_cwng.svg"
            },
            {
              title: "Kde",
              description:
                "Budova vyššího gymnázia Jarošky na třídě Kapitána Jaroše 14, 658 70, Brno",
              image:
                "https://uploads.divjoy.com/undraw-personal_settings_kihd.svg"
            },
            {
              title: "Kontakt",
              description: "dodělat",
              image: "https://uploads.divjoy.com/undraw-having_fun_iais.svg"
            },
            {
              title: "Název",
              description:
                "Oficiální název akce je BrNOC — brněnská přednášková noc, lze použít i zkratkové slovo BrNOC (tj. můžete jej libovolně skloňovat a odvozovat z něj, např. přídavné jméno BrNOCí). Prosíme, nepoužívejte ani BrNoC, ani Brnoc, možnou alternativou je ještě BRNOC.",
              image: "https://uploads.divjoy.com/undraw-balloons_vxx5.svg"
            }
          ]}
        ></Features2>
      </div>
    </Section>
  );
}

export default FeaturesSection2;
