import React, { useState } from "react";
import FormStatus from "./FormStatus";
import FormField from "./FormField";
import SectionButton from "./SectionButton";
import contact from "./../util/contact.js";
import { useForm } from "react-hook-form";

function Contact(props) {
  const [status, setStatus] = useState({ type: "none" });
  const { handleSubmit, register, errors, reset } = useForm();

  const onSubmit = ({ name, email, message }) => {
    setStatus({ type: "pending" });

    contact.submit({ name, email, message }).then(() => {
      reset(); // Reset form
      setStatus({
        type: "success",
        message: "Your message has been sent! We'll get back to you soon."
      });
    });
  };

  return (
    <>
      {status && status.message && (
        <FormStatus type={status.type} message={status.message}></FormStatus>
      )}

      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="field is-horizontal">
          <div className="field-body">
            {props.showNameField && (
              <FormField
                name="name"
                type="text"
                placeholder="Name"
                error={errors.name}
                inputRef={register({
                  required: "Please enter your name"
                })}
              ></FormField>
            )}

            <FormField
              name="email"
              type="email"
              placeholder="Email"
              error={errors.email}
              inputRef={register({
                required: "Please enter your email"
              })}
            ></FormField>
          </div>
        </div>
        <div className="field is-horizontal">
          <div className="field-body">
            <FormField
              name="message"
              type="textarea"
              placeholder="Message"
              rows={5}
              error={errors.message}
              inputRef={register({
                required: "Please enter a message"
              })}
            ></FormField>
          </div>
        </div>
        <div className="field is-horizontal">
          <div className="field-body">
            <div className="field">
              <div className="control">
                <SectionButton
                  parentColor={props.parentColor}
                  size="medium"
                  state={status.type === "pending" ? "loading" : "normal"}
                >
                  {props.buttonText}
                </SectionButton>
              </div>
            </div>
          </div>
        </div>
      </form>
    </>
  );
}

export default Contact;
