import React from "react";
import Section from "./Section";
import ContentCards from "./ContentCards";

function ContentCardsSection(props) {
  return (
    <Section
      color={props.color}
      size={props.size}
      backgroundImage={props.backgroundImage}
      backgroundImageOpacity={props.backgroundImageOpacity}
    >
      <div className="container">
        <ContentCards
          people={[
            {
              image: "https://source.unsplash.com/aHrxrT1q2h0/800x600",
              title: "Faucibus turpis in",
              body:
                "Purus semper eget duis at tellus at urna condimentum mattis. Non blandit massa enim nec.",
              url: "/post/golden-gate"
            },
            {
              image: "https://source.unsplash.com/BkmdKnuAZtw/800x600",
              title: "Faucibus turpis in",
              body:
                "Purus semper eget duis at tellus at urna condimentum mattis. Non blandit massa enim nec.",
              url: "/post/beach"
            },
            {
              image: "https://source.unsplash.com/3XN-BNRDUyY/800x600",
              title: "Faucibus turpis in",
              body:
                "Purus semper eget duis at tellus at urna condimentum mattis. Non blandit massa enim nec.",
              url: "/post/road"
            },
            {
              image: "https://source.unsplash.com/eOcyhe5-9sQ/800x600",
              title: "Faucibus turpis in",
              body:
                "Purus semper eget duis at tellus at urna condimentum mattis. Non blandit massa enim nec.",
              url: "/post/ballons"
            }
          ]}
        ></ContentCards>
      </div>
    </Section>
  );
}

export default ContentCardsSection;
