import React from "react";
import HeroSection from "./../components/HeroSection";
import FeaturesSection2 from "./../components/FeaturesSection2";
import FeaturesSection from "./../components/FeaturesSection";
import TeamBiosSection2 from "./../components/TeamBiosSection2";
import ClientsSection from "./../components/ClientsSection";
import ContentCardsSection from "./../components/ContentCardsSection";
import { useRouter } from "./../util/router.js";

function IndexPage(props) {
  const router = useRouter();

  return (
    <>
      <HeroSection
        color="black"
        size="medium"
        backgroundImage=""
        backgroundImageOpacity={1}
        title="BrNOC"
        subtitle="BrNOC je Brněnská přednášková noc, kde studenti přednášejí studentům o svých odborných pracích, zájmech anebo prostě o tématech, která jim připadají důležitá. Jedná se o největší akci svého druhu v ČR!"
        buttonText="Registrovat se"
        image="/eye.png"
        buttonOnClick={() => {
          // Navigate to pricing page
          router.push("/pricing");
        }}
      ></HeroSection>
      <FeaturesSection2
        color="black"
        size="medium"
        backgroundImage=""
        backgroundImageOpacity={1}
        title="O BrNOCi"
        subtitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud."
      ></FeaturesSection2>
      <FeaturesSection
        color="black"
        size="medium"
        backgroundImage=""
        backgroundImageOpacity={1}
        title="Harmonogram"
        subtitle="All the features you need to move faster"
      ></FeaturesSection>
      <TeamBiosSection2
        color="black"
        size="medium"
        backgroundImage=""
        backgroundImageOpacity={1}
        title="Meet the Team"
        subtitle=""
      ></TeamBiosSection2>
      <ClientsSection
        color="black"
        size="medium"
        backgroundImage=""
        backgroundImageOpacity={1}
        title="You're in good company"
        subtitle=""
      ></ClientsSection>
      <ContentCardsSection
        color="black"
        size="medium"
        backgroundImage=""
        backgroundImageOpacity={1}
        title="Featured Content"
        subtitle=""
      ></ContentCardsSection>
    </>
  );
}

export default IndexPage;
